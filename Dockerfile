FROM openjdk:8
ENV SBT_VERSION 1.4.7

RUN \
    curl -L -o sbt-$SBT_VERSION.deb http://scala.jfrog.io/artifactory/debian/sbt-$SBT_VERSION.deb && \
    dpkg -i sbt-$SBT_VERSION.deb && \
    rm sbt-$SBT_VERSION.deb && \
    apt-get update && \
    apt-get install sbt && \
    sbt -Dsbt.rootdir=true sbtVersion

WORKDIR /fusilli-io-postgresql

COPY . /fusilli-io-postgresql
CMD sbt run