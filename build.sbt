import Library._

name := "fusilli.io-postgresql"

version := "0.1"

scalaVersion := "2.13.5"

libraryDependencies ++= Seq(
  Slf4j,
  AkkaStream,
  AkkaStreamAlpakkaCSV,
  KafkaClient,
  OrgJson,
  akkaStreamAlpakkaSlick,
  OrgPostgres,
  Play,
  ApacheCommonsText
)