package io.fusilli.postgresql.services

import com.fasterxml.jackson.core.JsonParseException
import io.fusilli.postgresql.models.Models._
import io.fusilli.postgresql.services.KafkaServices.{getKafkaConsumer, getKafkaLogProducer, getKafkaProducer}
import io.fusilli.postgresql.services.LoggingServices.sendLog
import io.fusilli.postgresql.services.QueryFactory.conditionParser
import kafka.common.KafkaException
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.config.ConfigException
import org.apache.kafka.common.errors.NetworkException
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsNull, JsObject, JsString, JsValue, Json}

import java.io.FileNotFoundException
import java.sql.{Connection, DriverManager, ResultSet, Statement}
import java.time.Duration
import java.util
import scala.jdk.CollectionConverters.IterableHasAsScala

object PostgresServices {

  private final val log: Logger = LoggerFactory.getLogger(this.getClass.getName)

  def getSource(repoConfig: Repo, outputQueue: FusilliQueue): Unit = {
    try {
      val kafkaLogProducer = getKafkaLogProducer(outputQueue.host, outputQueue.port)

      sendLog(repoConfig.job_configuration_id, "START", kafkaLogProducer, "0", "")

      log.info("Retrieving source...")

      val dbAndSchema = repoConfig.schema_path.split("/")
      var db = ""
      var schema = ""
      if (dbAndSchema.size == 2) {
        db = dbAndSchema(0)
        schema = dbAndSchema(1) + "."
      } else {
        db = repoConfig.schema_path
      }

      log.info("Connecting to PostgreSQL...")

      val statement = createConnection(repoConfig, db)

      log.info("Connection with PostgreSQL established.")

      val attrs = repoConfig.entities(0).attributes.map(attr => attr.attr_name)
      var query: String = s"SELECT ${attrs.mkString(",")} FROM ${schema + repoConfig.entities(0).entity_name}"

      if (repoConfig.config != null) {
        query = query + complexQuery(repoConfig.config)
      }

      log.info("Executing SELECT query...")

      val result: ResultSet = statement.executeQuery(query)
      val producer: KafkaProducer[String, String] = getKafkaProducer(outputQueue.host, outputQueue.port)

      while (result.next()) {
        val out = for (k <- attrs) yield k -> JsString(result.getString(k))
        val row = JsObject(out.toMap)
        val newJsonMessage = Json.toJson(new JsonMessage(new Header("running"), row)).toString
        producer.send(new ProducerRecord[String, String](outputQueue.topic, "keyName", newJsonMessage.toString))
      }

      val end = Json.toJson(new JsonMessage(new Header("end"), JsString(""))).toString

      producer.send(new ProducerRecord[String, String](outputQueue.topic, "keyName", end))
      sendLog(repoConfig.job_configuration_id, "END", kafkaLogProducer, "100", "")
      statement.close()
    }
    catch {
      case f: FileNotFoundException => log.error("Unable to find file.")
      case k: KafkaException => log.error("Unable to connect to Kafka.")
      case c: ConfigException => log.error("Unable to retrieve config for Kafka")
    }
  }

  def getSink(repoConfig: Repo, inputQueue: FusilliQueue): Unit = {
    try {
      val kafkaLogProducer = getKafkaLogProducer(inputQueue.host, inputQueue.port)

      sendLog(repoConfig.job_configuration_id, "START", kafkaLogProducer, "0", "")

      log.info("Retrieving sink...")

      val dbAndSchema = repoConfig.schema_path.split("/")
      var db = ""
      var schema = ""
      if (dbAndSchema.size == 2) {
        db = dbAndSchema(0)
        schema = dbAndSchema(1) + "."
      } else {
        db = repoConfig.schema_path
      }

      log.info("Connecting to PostgreSQL...")

      val statement = createConnection(repoConfig, db)

      log.info("Connection with PostgreSQL established.")

      var entityName: String = ""

      if (repoConfig.new_entity.name.equals("")) {
        entityName = repoConfig.entities(0).entity_name
      }
      else {
        entityName = repoConfig.new_entity.name
      }

      var attrsInsert: Seq[(String, String)] = null

      var i = 0;
      var j = 0;
      while (i < repoConfig.entities.length) {
        while (j < repoConfig.entities(i).attributes) {
          attrsInsert = repoConfig.entities(i).attributes.map(attr => (attr.attr_name, attr.attr_type))
          j = j + 1
        }
        i = i + 1
      }

      // val attrsInsert = repoConfig.entities(0).attributes.map(attr => (attr.attr_name, attr.attr_type))

      val createQuery: String = s"""CREATE TABLE IF NOT EXISTS ${schema + entityName} (${attrsInsert.map(elem => "%s %s".format(elem._1, if (elem._2 == "DOUBLE") elem._2.replace("DOUBLE", "DOUBLE PRECISION") else elem._2)).mkString(", ")})"""
      log.info("query: " + createQuery)
      log.info("Executing CREATE TABLE query...")
      statement.execute(createQuery)

      val consumer: KafkaConsumer[String, String] = getKafkaConsumer(inputQueue.host, inputQueue.port, inputQueue.group_id)
      consumer.subscribe(util.Arrays.asList(inputQueue.topic))

      var cycle = true

      while (cycle) {
        val record = consumer.poll(Duration.ofMillis(1000)).asScala //this is used to read a line in the queue
        for (data <- record.iterator) {
          val jsonMessage = Json.parse(data.value()).as[JsonMessage]

          if (!jsonMessage.header.status.equals("end")) {
            val json = jsonMessage.data.as[JsObject]
            try {
              log.info("Executing INSERT query...")
              statement.execute(insertElem(repoConfig.entities(0), schema, json))
              log.info("INSERT query completed")
            } catch {
              case _: NullPointerException => log.error("Unable to insert row or not found")
            }
          } else {
            log.info("End received.")
            cycle = false
          }
        }
      }
      sendLog(repoConfig.job_configuration_id, "END", kafkaLogProducer, "100", "END PIPELINE")
      log.info("All rows inserted.")

    } catch {
      case f: FileNotFoundException => log.error("Unable to find file.")
      case k: KafkaException => log.error("Unable to connect to Kafka.")
      case c: ConfigException => log.error("Unable to retrieve config for Kafka")
      case n: NetworkException => log.error("Unable to find Network.")
      case j: JsonParseException => log.error("Unable to parse row")
    }
  }

  def createConnection(repo: Repo, db: String): Statement = {
    val url: String = s"jdbc:postgresql://${repo.host}:${repo.port}/${db}?sslmode=require"

    Class.forName("org.postgresql.Driver");

    val conn: Connection = DriverManager.getConnection(url, repo.repo_user, repo.repo_password);

    conn.createStatement();
  }

  def insertElem(item: Entity, schema: String, json: JsObject): String = {
    var insertQuery = s"INSERT INTO ${schema + item.entity_name}(${item.attributes.map(_.attr_name).mkString(",")}) VALUES("

    for (element <- item.attributes) {
      element.attr_type match {
        case "INT" => insertQuery += s"${(json \ element.attr_name).as[Int]}, "
        case "BIGINT" => insertQuery += s"${(json \ element.attr_name).as[Int]}, "
        case "NUMERIC" => insertQuery += s"${(json \ element.attr_name).as[Double]}, "
        case _ => insertQuery += s"'${(json \ element.attr_name).as[String].replace("\'", "\'\'")}', "
      }
    }
    insertQuery = insertQuery.patch(insertQuery.lastIndexOf(','), ")", 1)
    insertQuery
  }

  def complexQuery(config: JsValue): String = {

    var complexQuery = ""
    val parsedConfig = config.as[JsObject]
    if (parsedConfig.keys.contains("where_condition")) {
      val whereCond = (parsedConfig \ "where_condition").get.as[JsValue]
      complexQuery = " WHERE " + conditionParser(whereCond)
    }
    if (parsedConfig.keys.contains("group_by_condition")) {
      complexQuery = complexQuery + " GROUP BY " //TODO
    }
    if (parsedConfig.keys.contains("order_by_condition")) {
      complexQuery = complexQuery + " ORDER BY " // TODO
    }

    complexQuery
  }
}