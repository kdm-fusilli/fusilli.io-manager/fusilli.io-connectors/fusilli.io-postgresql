package io.fusilli.postgresql.services

import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.slf4j.{Logger, LoggerFactory}

import java.util.Properties

object KafkaServices {
  private final val log: Logger = LoggerFactory.getLogger(this.getClass.getName)

  def getKafkaLogProducer(host: String, port: Int): KafkaProducer[String, String] = {
    log.info("Retrieving Kafka Log Producer...")
    val kafkaLogProducerProps = new Properties()
    kafkaLogProducerProps.put("bootstrap.servers", s"${host}:${port}")
    kafkaLogProducerProps.put("key.serializer", classOf[StringSerializer].getName)
    kafkaLogProducerProps.put("value.serializer", classOf[StringSerializer].getName)
    log.info("Kafka Log Producer created ...")
    new KafkaProducer[String, String](kafkaLogProducerProps)
  }

  def getKafkaProducer(host: String, port: Int): KafkaProducer[String, String] = {
    log.info("Retrieving Kafka Producer...")
    val kafkaProducerProps = new Properties()
    kafkaProducerProps.put("bootstrap.servers", s"${host}:${port}")
    kafkaProducerProps.put("key.serializer", classOf[StringSerializer].getName)
    kafkaProducerProps.put("value.serializer", classOf[StringSerializer].getName)
    log.info("Kafka Producer created ...")
    new KafkaProducer[String, String](kafkaProducerProps)
  }

  def getKafkaConsumer(host: String, port: Int, group_id: String): KafkaConsumer[String, String] = {
    log.info("Retrieving Kafka Consumer...")
    val kafkaConsumerProperties = new Properties()
    kafkaConsumerProperties.put("bootstrap.servers", s"${host}:${port}")
    kafkaConsumerProperties.put("key.deserializer", classOf[StringDeserializer].getName)
    kafkaConsumerProperties.put("value.deserializer", classOf[StringDeserializer].getName)
    kafkaConsumerProperties.put("auto.offset.reset", "latest")
    kafkaConsumerProperties.put("group.id", group_id)

    log.info("Creating Kafka Consumer...")
    new KafkaConsumer[String, String](kafkaConsumerProperties)
  }
}