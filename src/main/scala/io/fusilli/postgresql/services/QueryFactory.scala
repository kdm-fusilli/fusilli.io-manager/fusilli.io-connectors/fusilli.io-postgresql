package io.fusilli.postgresql.services

import play.api.libs.json.{JsArray, JsObject, JsValue, Json}

object QueryFactory {

  def conditionParser(jsx: JsValue): String = {
    val js = jsx.as[JsObject]
    if (js.keys.contains("cond_type")) {
      val fieldType = (js \ "cond_type").get.as[String]
      val verify = (js \ "verify").get.as[String]
      val key = (js \ "key").get.as[String]
      fieldType match {
        case "string" =>
          val value = (js \ "value").get.as[String]
          createStringCondition(verify, key, value)

        case "number" =>
          val value = (js \ "value").get.as[Int]
          createIntCondition(verify, key, value)
      }
    }
    else {
      val conditionSeq = (js \ "apply_to").get.as[JsArray].value
      createComplexQuery((js \ "fun").get.as[String], conditionSeq.toList)
    }
  }

  def createStringCondition(verify: String, key: String, value: String): String = {
    val logicalFun: String = verify match {
      case "equals_sensitive" => " = "
      case "contains_sensitive" => " ~ "
      case "not_contains_sensitive" => " !~ "
      case "equals_insensitive" => " ILIKE "
      case "contains_insensitive" => " ~* "
      case "not_contains_insensitive" => " !~* "
    }
    s" ( ${key} ${logicalFun} '${value}' ) "
  }

  def createIntCondition(verify: String, key: String, value: Int): String = {
    val logicalFun: String = verify match {
      case "equals" => " = "
      case "less" => " < "
      case "greater" => " > "
      case "less_equals" => " <= "
      case "greater_equals" => " >= "
      case "not_equals" => " != "
    }
    s" ( ${key} ${logicalFun} ${value} ) "
  }

  def createComplexQuery(fun: String, subordinates: List[JsValue]): String = {
    var outQuery: String = ""
    var firstCycle = true
    for (cond <- subordinates) {
      if (firstCycle) {
        outQuery = conditionParser(Json.parse(cond.toString()))
        firstCycle = false
      }
      else {
        outQuery = outQuery + fun + conditionParser(Json.parse(cond.toString()))
      }
    }
    outQuery
  }
}