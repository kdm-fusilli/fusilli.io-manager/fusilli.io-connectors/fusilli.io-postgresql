package io.fusilli.postgresql.services

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.json.JSONObject

object LoggingServices {

  def sendLog(jobConfigurationId: Int, status: String, logProducer: KafkaProducer[String, String], percentage: String, message: String): Unit = {

    val logQueue = "fusilli_pipelines_logger"

    val instanceId: String = System.getenv("""instance_id""")
    val pipelineId: String = System.getenv("""pipeline_id""")
    val policiesId: String = System.getenv("""policies_id""")
    val version: String = System.getenv("""version""")

    val jsonLogger: JSONObject = new JSONObject()
    jsonLogger.put("instance_id", instanceId)
    jsonLogger.put("pipeline_id", pipelineId)
    jsonLogger.put("policies_id", policiesId)
    jsonLogger.put("version", version)
    jsonLogger.put("job_configuration_id", jobConfigurationId.toString)
    jsonLogger.put("status", status)
    jsonLogger.put("message", message)
    jsonLogger.put("timestamp", System.currentTimeMillis().toString)
    jsonLogger.put("percentage", percentage)

    logProducer.send(new ProducerRecord[String, String](logQueue, "keyName", jsonLogger.toString))
  }
}