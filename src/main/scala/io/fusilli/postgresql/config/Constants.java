package io.fusilli.postgresql.config;

public enum Constants {

    input_edges,
    output_edges,
    connector_config,
    log_queue
}