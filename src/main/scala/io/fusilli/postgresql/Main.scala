package io.fusilli.postgresql

import io.fusilli.postgresql.config.ConfigApp
import io.fusilli.postgresql.services.PostgresServices
import org.slf4j.{Logger, LoggerFactory}

object Main extends App {

  private final val log: Logger = LoggerFactory.getLogger(this.getClass.getName)

  val connectorConfig = ConfigApp.getConnectorConfigJson()

  if (connectorConfig.purpose.contains("input")) {
    val outputQueue = ConfigApp.getOutputConfigJson()
    PostgresServices.getSource(connectorConfig, outputQueue)

  } else {
    val inputQueue = ConfigApp.getInputConfigJson()
    PostgresServices.getSink(connectorConfig, inputQueue)
  }
}