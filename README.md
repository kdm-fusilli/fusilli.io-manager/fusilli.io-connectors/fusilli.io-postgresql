# fusilli.io-postgresql

## Introduction

The PostgreSQL connector makes a connection to the postgresql databases.
It takes the data, with the attributes present in the configuration, and inserts it into the json sent to the kafka queue, consisting of header and data

```bash
{"header":{"status":"running"},"data":{"PROVINCIA" : "VT", "LUNGHEZZA_TRATTA_FO" : "6050", "COMUNE" : "Ronciglione", "CODICE_INTERVENTO" : "LARSFARONCRONCMA", "PROPRIETARIO_FO" : "REGIONE LAZIO", "DESCRIZIONE_INTERVENTO" : "Ronciglione - Ronciglione MA0018"}}
```

At the end of the connector the message sent to the queue will be the following:

```bash
{"header":{"status":"end"},"data":""}
```

## How to install scala

Download the Scala and follow the further instructions for the installation of Scala. However, one can easily install latest version of Scala on Ubuntu with the use of following command:

```bash
sudo apt-get install scala
```

## How to install sbt

Download sbt follow the steps in the official site documentation:

```bash
https://www.scala-sbt.org/
```

## Execution of job via IDE

First step is to expose all enviroment variables:

```bash
export input_edges='{"input_edges":[{"label":"String","host":"String","port":"Int","group_id":"String","topic":"String","user":"String","password":"String"}]}'

export output_edges='{"output_edges":[{"label":"String","host":"String","port":"Int","group_id":"String","topic":"String","user":"String","password":"String"}]}'

export connector_config='{"job_configuration_id":1,"source_id":1,"repo_name":"","repo_user":"String","repo_password":"String","repo_type":"postgresql","host":"localhost","port":5432,"schema_path":"fusilli_target","delay":100,"purpose":"output","entities":[{"entity_id":10,"connection_edge":1,"entity_name":"test","entity_type":"SQL","separator":";","entity_instances":1,"attributes":[{"attr_name":"PROVINCIA","attr_type":"VARCHAR(250)","format":"","length":""},{"attr_name":"COMUNE","attr_type":"VARCHAR(250)","format":"","length":""},{"attr_name":"CODICE_INTERVENTO","attr_type":"VARCHAR(50)","format":"","length":""},{"attr_name":"DESCRIZIONE_INTERVENTO","attr_type":"VARCHAR(50)","format":"","length":""},{"attr_name":"LUNGHEZZA_TRATTA_FO","attr_type":"INTEGER","format":"","length":""},{"attr_name":"PROPRIETARIO_FO","attr_type":"VARCHAR(50)","format":"","length":""}]}],"config":{"where_condition":{"fun":"OR","apply_to":[{"cond_type":"string","key":"CODICE_INTERVENTO","value":"LAISFBAGNAIAGD01","verify":"equals"},{"cond_type":"string","key":"CODICE_INTERVENTO","value":"2$","verify":"contains"}]}}}'

```

To test locally the project in debug mode run the following command inside the project folder:

```bash
sbt -jvm-debug 5005 run
```

## Execution of job via Docker

Run the following command inside the project folder:

```bash
docker-compose up -f docker/docker-compose.yml
```

This is an example file of docker-compose.yml:

```bash
version: '2.0'
services:
    postgresqlconnector:
        container_name: postgresqlconnector
        image: registry.gitlab.com/kdm-fusilli/fusilli.io-manager/fusilli.io-connectors/fusilli.io-postgresql:latest
        restart: on-failure
        command: bash -c "sbt run"
        environment:
            input_edges: '{"input_edges":[{"label":"String","host":"String","port":"Int","group_id":"String","topic":"String","user":"String","password":"String"}]}'
            output_edges: '{"output_edges":[{"label":"String","host":"String","port":"Int","group_id":"String","topic":"String","user":"String","password":"String"}]}'
            connector_config: '{"job_configuration_id":1,"source_id":1,"repo_name":"","repo_user":"String","repo_password":"String","repo_type":"postgresql","host":"localhost","new_entity":{"name":"new_entity_name"},"port":5432,"schema_path":"fusilli_target","delay":100,"purpose":"output","entities":[{"entity_id":10,"connection_edge":1,"entity_name":"test","entity_type":"SQL","separator":";","entity_instances":1,"attributes":[{"attr_name":"PROVINCIA","attr_type":"VARCHAR(250)","format":"","length":""},{"attr_name":"COMUNE","attr_type":"VARCHAR(250)","format":"","length":""},{"attr_name":"CODICE_INTERVENTO","attr_type":"VARCHAR(50)","format":"","length":""},{"attr_name":"DESCRIZIONE_INTERVENTO","attr_type":"VARCHAR(50)","format":"","length":""},{"attr_name":"LUNGHEZZA_TRATTA_FO","attr_type":"INTEGER","format":"","length":""},{"attr_name":"PROPRIETARIO_FO","attr_type":"VARCHAR(50)","format":"","length":""}]}],"config":{"where_condition":{"fun":"OR","apply_to":[{"cond_type":"string","key":"CODICE_INTERVENTO","value":"LAISFBAGNAIAGD01","verify":"equals"},{"cond_type":"string","key":"CODICE_INTERVENTO","value":"2$","verify":"contains"}]}}}'
        ports:
          - 8080:8080
        network_mode: "host"
```

To check the environment variables set in a docker image use the following command:

```bash
docker exec <continer_id> env
```

To check the container logs use the following command:

```bash
docker logs <continer_id>
```

