import sbt._

// format: OFF
object Version {

  val Log4j                         = "1.7.30"
  val Akka                          = "2.6.9"
  val AkkaStreamAlpakkaFtp          = "2.0.2"
  val AkkaStreamAlpakkaCSV          = "2.0.1"
  val KafkaClients                  = "2.7.0"
  val OrgJson                       = "20210307"
  val akkaStreamAlpakkaSlick        = "2.0.1"
  val OrgPostgres                   = "42.2.11"
  val slick                         = "2.0.1"
  val Play                          = "2.9.2"
  val ApacheCommonsTextVersion      = "1.9"
}

object Library {
  // External Common Libraries
  val Slf4j                   = "org.slf4j"                       % "slf4j-simple"                       % Version.Log4j
  val AkkaStream              = "com.typesafe.akka"               %% "akka-stream"                       % Version.Akka
  val AkkaStreamAlpakkaCSV    = "com.lightbend.akka"              %% "akka-stream-alpakka-csv"           % Version.AkkaStreamAlpakkaCSV
  val akkaStreamAlpakkaSlick  = "com.lightbend.akka"              %% "akka-stream-alpakka-slick"         % Version.akkaStreamAlpakkaSlick
  val KafkaClient             = "org.apache.kafka"                %% "kafka"                             % Version.KafkaClients
  val OrgJson                 = "org.json"                        % "json"                               % Version.OrgJson
  val Play                    = "com.typesafe.play"               %% "play-json"                         % Version.Play
  val ApacheCommonsText       = "org.apache.commons"              % "commons-text"                       % Version.ApacheCommonsTextVersion
  // External Specific Libraries
  val OrgPostgres             = "org.postgresql"                  % "postgresql"                         % Version.OrgPostgres
}

// format: ON
// ,
//  "